<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

		<!-- Twitter meta-tags -->
		<meta name="twitter:card" content="${twitter_card}"/>
		<meta name="twitter:site" content="${twitter_site}"/>
		<meta name="twitter:title" content="${twitter_title}"/>
		<meta name="twitter:description" content="${twitter_description}"/>
		<meta name="twitter:image"  content="${twitter_image}"/>
		<meta name="twitter:url"  content="${twitter_url}"/>
		<!-- Twitter meta-tags -->

		<script src="<c:url value="/resources/js/jquery-3.1.0.min.js"/>" type="text/javascript"></script>

		<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/css/bootstrap-reset.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">

	    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
	    <link href="<c:url value="/resources/css/style-responsive.css"/>" rel="stylesheet"/>
	    <link href="<c:url value="/resources/css/lightbox.css"/>" rel="stylesheet">
	    <link href="<c:url value="/resources/css/custom_font.css"/>" rel="stylesheet" >
	    <!--[if lt IE 8]>
	    <link href="<c:url value="/resources/css/custom_font_ie7.css"/>" rel="stylesheet" >
	    <!--<![endif]-->
		<script type="text/javascript">
			$(document).ready(function(){
				setImage();
			});

			function setImage() {
				if ( "${photo_id}" == "default") {
					$("#main_image").attr("src","<c:url value="/resources/images/pool_top.png"/>");				
				} else {
					$("#main_image").attr("src","http://imgur.com/${photo_id}.png");
				}				
			}
		</script>
	</head>
	<body class="fixed-width">
		<section id="container" class="">
			<header class="header fixed-top clearfix">
				<div class="brand-centered">
					<a href="http://www.smarth2o-fp7.eu" target="_blank">
						<img src="<c:url value="/resources/images/logo.png"/>"/>
					</a>
				</div>
			</header>
			<section id="main-content" style="background-color: #d9edf7">
				<section class="wrapper" id="hp-presentation">
					<div  class="row">
						<div  class="col-md-6 col-md-offset-1">
							<h2 class="text-left ">Welcome to the<strong> SmartH2O Images portal</strong></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5 col-md-offset-1">
							<h4>
							<strong>Join the community</strong> of smart water savers. Know your consumption and learn how to save more.</h4>
							<h4>
							This version is only the first one. We will continue to improve your experience, for making water saving fun. Stay tuned for more news very soon!
							</h4>
						</div>
					</div>
					<div class="row">
						<div id="divContainer" class="photo-well-scrappy-view" style="height: 500px;">
							<div class="absolute-center">
								<img id="main_image" class="main-photo" width="400" height="400" src="<c:url value="/resources/images/logo.png"/>">
							</div>
						</div>					
					</div>					
				</section>
			</section>
			<footer class="footer-section" style="height:90px;">
				<div class="pull-left margin-left">
						<img src="<c:url value="/resources/images/ec_logo.png"/>" style="display:inline-block; height:75px;"/>
						<div style="display:inline-block; margin-left:5px; width:50%; vertical-align:middle;">
						The SmartH2O project receives funding from the European Commission and is part of the ICT4Water cluster
						</div>
					</div>
					<div class="pull-right margin-right">
						<a href="http://www.smarth2o-fp7.eu" target="_blank" style="color:white;">2015 © SmartH2O Project  </a>
						<div class="activity-icon " style="display:inline-block; margin-left:5px;">
							<a target="_blank" href="https://twitter.com/smarth2oproject">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
					</div>
			</footer>
		</section>
	</body>
</html>