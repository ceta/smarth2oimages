package com.smarth2o.community.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class ImageController {

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(method=RequestMethod.GET)
	public String getAll(Model model, Locale locale) {
		return showImage("en", "default", model, locale);
	}

	@RequestMapping("/{language}/{photo_id}")
	public String showImage(@PathVariable("language") String language, 
							@PathVariable("photo_id") String photo_id, 
							Model model, Locale locale) {
		model.addAttribute("photo_id", photo_id);
		model.addAttribute("country", messageSource.getMessage("country", null, new Locale(language)));
		model.addAttribute("twitter_card", messageSource.getMessage("twitter_card", null, new Locale(language)));
		model.addAttribute("twitter_site", messageSource.getMessage("twitter_site", null, new Locale(language)));
		model.addAttribute("twitter_title", messageSource.getMessage("twitter_title", null, new Locale(language)));
		model.addAttribute("twitter_description", messageSource.getMessage("twitter_description", null, new Locale(language)));
		model.addAttribute("twitter_image", messageSource.getMessage("twitter_image", new Object[]{photo_id}, new Locale(language)));
		model.addAttribute("twitter_url", messageSource.getMessage("twitter_url", null, new Locale(language)));

		return "index";
	}

}
